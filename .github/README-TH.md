# ชุดข้อมูล Coronavirus Covid-19 ของประเทศสหรัฐอเมริกา
> **For `README.md` in English, please see [README.md](README.md)**

ข้อมูล [Coronavirus Covid-19](https://www.kaggle.com/datasets/yasirabdaali/corona-virus-covid19-us-counties) เป็นชุดข้อมูลที่รวบรวมข้อมูลการติดเชื้อโควิด-19 ในประเทศสหรัฐอเมริกา โดยมีข้อมูลดังนี้
- จำนวนผู้ติดเชื้อยืนยัน
- จำนวนผู้เสียชีวิต 
- จำนวนผู้ที่ฟื้นตัวจากโรคติดเชื้อโควิด-19
- เมือง / รัฐที่ทำการบันทึกข้อมูล
- ตำแหน่งที่ตั้ง (`Latitude (ละติจูด)` , `Longitude (ลองจิจูด)`)

คำแนะนำอย่างละเอียดอ่านต่อที่ [prerequisites.md](docs/prerequisites.md) (เป็นภาษาอังกฤษ)

## สามาชิก
- นาย กังวานทรัพย์ แซ่ลี้ (ก้อง) data vitualize และ หา/ทำความสะอาดชุดข้อมูลเพิ่มเติมที่ใช้ในการเปรียบเทียบ
- นาย ปองภพ เชื้อประเสริฐศักดิ์ (พัตเตอร์) data vitualize นำเสนอในรูปกราฟแท่ง
- นาย ชัยภัทร ใจน่าน (ไอซ์) QA/Testing, Document writer and Data Analysts
 
## ประกาศ

การใช้ชุดข้อมูล `yasirabdaali/corona-virus-covid19-us-counties` ในโปรเจคนี้ขึ้นอยู่กับข้อกำหนดและเงื่อนไขของการอนุญาตของชุดข้อมูลเอง ตามที่ระบุใน [CC0: Public Domain](https://creativecommons.org/publicdomain/zero/1.0/) 

การใช้งานโค้ดในโปรเจคนี้เพื่อวัตถุประสงค์การศึกษา สามารถนำไปใช้งาน แก้ไข และแจกจ่ายต่อได้โดยไม่มีข้อจำกัด `แต่กรุณาให้เครดิตแก่ผู้เขียนโค้ดด้วยนะครับ`

### Disclaimer:

ผู้เขียนโปรเจค (Covid-19US-CS203) ต้องการให้ความสำคัญกับการอ้างอิงแหล่งข้อมูล และไม่รับผิดชอบใดๆ ทั้งสิ้น ในกรณีที่เกิดข้อพิพาทหรือปัญหาใดๆ จากการใช้งานข้อมูล ผู้ใช้งานควรปฏิบัติตามข้อกำหนดและเงื่อนไขการใช้งานของแหล่งข้อมูล และให้ความเคารพต่อผู้สร้างข้อมูลเสมอ

แหล่งข้อมูล: [Kaggle: yasirabdaali/corona-virus-covid19-us-counties](https://www.kaggle.com/datasets/yasirabdaali/corona-virus-covid19-us-counties).

---
Get help: [Post in our discussion board](https://github.com/AppleBoiy/Covid-19US-CS203/discussions)

&copy; 2023 AppleBoiy &bull; [Code of Conduct](CODE_OF_CONDUCT.md) &bull; [GPL-3.0 license](../LICENSE)
