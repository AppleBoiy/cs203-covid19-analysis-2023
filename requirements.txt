numpy==1.24.3
matplotlib==3.7.1
pandas==1.5.3
requests==2.31.0
folium==0.14.0
scipy==1.10.1
ipython==8.12.0
